from django.db import models
from django.conf import settings


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    members = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        related_name="projects",
    )
    # this model should implicitly convert to a
    # string using __str__ this is value of the name property

    def __str__(self):
        return self.name
