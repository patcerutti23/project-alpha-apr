from django.contrib import admin
from projects.models import Project

# to see in the django admin site


class ProjectAdmin(admin.ModelAdmin):
    pass


admin.site.register(Project, ProjectAdmin)
