from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login

# will create a new user account from their username and password


def signup(request):
    if request.method == "POST":
        # Post when someone selects submit
        form = UserCreationForm(request.POST)
        # request.POST is a dictionary of all the stuff
        # that was posted to us from the form
        # GET when someone visits the page
        if form.is_valid():
            # Create the user
            # you could also do it this way without line 17-21
            # user = form.save()
            username = request.POST.get("username")
            password = request.POST.get("password1")
            user = User.objects.create_user(
                username=username,
                password=password,
            )
            # Login the user
            user.save()
            login(request, user)
            # Redirect them somewhere?
            return redirect("home")
    else:
        form = UserCreationForm()
    context = {
        "form": form,
    }
    # sending back a blank form bc going to a url in the browser is going to be a "GET"
    return render(request, "registration/signup.html", context)
